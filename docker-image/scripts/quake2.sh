#!/bin/bash

if [ "$GAME" != 'ctf' ] && [ "$GAME" != 'dm64' ] && [ "$GAME" != 'mp1' ] && [ "$GAME" != 'mp2' ]; then
    GAME=dm
fi

./quake2 +set dedicated 1 +exec server.cfg +exec $GAME.cfg
