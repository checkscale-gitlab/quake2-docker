#!/usr/bin/bash

DIRECTORY="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
NAME='quake2'

# Make real config files out of the examples (if they don't already exist)
for examplefile in "$DIRECTORY"/config/*.cfg.example; do
    configfile=${examplefile%.example}

    if [ ! -f "$configfile" ]; then
        cp -p "$examplefile" "$configfile"
    fi
done

docker run \
    -e "GAME=$1" \
    -i \
    --name "$NAME" \
    -p ${IP_ADDRESS}:27910:27910/udp \
    --rm \
    -t \
    -v "$DIRECTORY/config":/usr/local/games/quake2/config \
    "$NAME"
