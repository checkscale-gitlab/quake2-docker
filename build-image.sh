#!/usr/bin/bash

GAMES=( BASEQ2 XATRIX ROGUE )
PAK_FILE_BASEQ2='pak_files/baseq2/pak0.pak'
PAK_MD5SUM_BASEQ2='1ec55a724dc3109fd50dde71ab581d70'
PAK_FILE_XATRIX='pak_files/xatrix/pak0.pak'
PAK_MD5SUM_XATRIX='f5e7b04f7d6b9530c59c5e1daa873b51'
PAK_FILE_ROGUE='pak_files/rogue/pak0.pak'
PAK_MD5SUM_ROGUE='5e2ecbe9287152a1e6e0d77b3f47dcb2'

# Switch to the absolute path to the docker-images directory, in case this
# script is being run from somewhere else.
cd "$(dirname "${BASH_SOURCE[0]}")/docker-image"

# Check required/optional pak files
echo "Checking PAK files..."

for game in "${GAMES[@]}"; do
    pak_file_var="PAK_FILE_$game"
    pak_md5sum_var="PAK_MD5SUM_$game"

    if [ ! -f "${!pak_file_var}" ]; then
        # The baseq2 pak file is required, but the ones from the mission packs are optional.
        if [ "$game" != "BASEQ2" ]; then
            continue
        fi

        echo "You need the \"pak0.pak\" file from the full version of Quake II!"
        echo "Please copy the file to \"docker-image/$PAK_FILE_BASEQ2/\" and try again."

        exit 1
    fi

    if [ `md5sum "${!pak_file_var}" | awk '{print $1}'` != "${!pak_md5sum_var}" ]; then
        echo "The \"${!pak_file_var}\" file is not the correct version! Please replace it and try again."
        echo "The MD5sum should be: ${!pak_md5sum_var}"

        exit 1
    fi
done

# Build the image
docker build -t quake2 .
